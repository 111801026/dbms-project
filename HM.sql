-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: Hospital_Management
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB-1:10.5.9+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allocated`
--

DROP TABLE IF EXISTS `allocated`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `allocated` (
  `registered_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`registered_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `allocated_ibfk_1` FOREIGN KEY (`registered_id`) REFERENCES `in_patient` (`registered_id`),
  CONSTRAINT `allocated_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allocated`
--

LOCK TABLES `allocated` WRITE;
/*!40000 ALTER TABLE `allocated` DISABLE KEYS */;
INSERT INTO `allocated` VALUES (3,1),(1,2),(2,3);
/*!40000 ALTER TABLE `allocated` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assigned_doctor`
--

DROP TABLE IF EXISTS `assigned_doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigned_doctor` (
  `patient_id` int(11) DEFAULT NULL,
  `doctor_id` varchar(5) DEFAULT NULL,
  KEY `patient_id` (`patient_id`),
  KEY `doctor_id` (`doctor_id`),
  CONSTRAINT `assigned_doctor_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_doctor_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`doctor_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigned_doctor`
--

LOCK TABLES `assigned_doctor` WRITE;
/*!40000 ALTER TABLE `assigned_doctor` DISABLE KEYS */;
INSERT INTO `assigned_doctor` VALUES (1,'D002'),(2,'D001'),(3,'D003');
/*!40000 ALTER TABLE `assigned_doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assigned_nurse`
--

DROP TABLE IF EXISTS `assigned_nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assigned_nurse` (
  `registered_id` int(11) NOT NULL,
  `nurse_id` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`registered_id`),
  KEY `nurse_id` (`nurse_id`),
  CONSTRAINT `assigned_nurse_ibfk_1` FOREIGN KEY (`registered_id`) REFERENCES `in_patient` (`registered_id`),
  CONSTRAINT `assigned_nurse_ibfk_2` FOREIGN KEY (`nurse_id`) REFERENCES `nurse` (`nurse_id`),
  CONSTRAINT `assigned_nurse_ibfk_3` FOREIGN KEY (`nurse_id`) REFERENCES `nurse` (`nurse_id`) ON DELETE CASCADE,
  CONSTRAINT `assigned_nurse_ibfk_4` FOREIGN KEY (`registered_id`) REFERENCES `in_patient` (`registered_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assigned_nurse`
--

LOCK TABLES `assigned_nurse` WRITE;
/*!40000 ALTER TABLE `assigned_nurse` DISABLE KEYS */;
INSERT INTO `assigned_nurse` VALUES (1,'N001'),(3,'N002'),(2,'N003');
/*!40000 ALTER TABLE `assigned_nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `bill_id` int(11) NOT NULL,
  `room_cost` decimal(10,2) DEFAULT 0.00,
  `medicine_cost` decimal(10,2) DEFAULT 0.00,
  `other_cost` decimal(5,2) DEFAULT 0.00,
  `test_cost` decimal(6,2) DEFAULT 0.00,
  `total_cost` decimal(20,2) DEFAULT 0.00,
  `bill_date` datetime NOT NULL,
  PRIMARY KEY (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
INSERT INTO `bill` VALUES (1,20.00,30.00,40.00,50.00,140.00,'1996-06-04 00:00:00'),(2,100.00,200.00,300.00,400.00,1000.00,'1996-06-03 00:00:00'),(3,100.00,200.00,300.00,400.00,1000.00,'1996-06-04 00:00:00');
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(50) NOT NULL,
  `dept_head_id` varchar(50) NOT NULL,
  PRIMARY KEY (`department_id`),
  KEY `fk1` (`dept_head_id`),
  CONSTRAINT `fk1` FOREIGN KEY (`dept_head_id`) REFERENCES `works` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
INSERT INTO `department` VALUES (1,'Department1','D001'),(2,'Department2','N001'),(3,'Department3','P001');
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctors`
--

DROP TABLE IF EXISTS `doctors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctors` (
  `doctor_id` varchar(5) NOT NULL CHECK (`doctor_id` like 'D%'),
  `qualification` varchar(20) NOT NULL,
  `consultation_charge` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`doctor_id`),
  CONSTRAINT `doctors_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctors`
--

LOCK TABLES `doctors` WRITE;
/*!40000 ALTER TABLE `doctors` DISABLE KEYS */;
INSERT INTO `doctors` VALUES ('D001','MBBS',200.00),('D002','BMBS',150.00),('D003','MBChB',300.00);
/*!40000 ALTER TABLE `doctors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `employee_id` varchar(5) NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(20) NOT NULL,
  `address` varchar(40) DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT 0.00,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES ('D001',1,'Employee1','1988-04-12','employee1@gmail.com','EmployeeStreet1',35000.00),('D002',0,'Employee2','1993-08-25','employee2@gmail.com','EmployeeStreet2',15000.00),('D003',0,'Employee3','1990-11-04','employee3@gmail.com','EmployeeStreet3',25000.00),('N001',0,'Employee6','1990-11-04','employee6@gmail.com','EmployeeStreet6',25000.00),('N002',0,'Employee7','1990-11-04','employee7@gmail.com','EmployeeStreet7',25000.00),('N003',0,'Employee8','1990-11-04','employee8@gmail.com','EmployeeStreet8',25000.00),('P001',0,'Employee4','1990-11-04','employee4@gmail.com','EmployeeStreet4',25000.00),('P002',0,'Employee9','1990-11-04','employee9@gmail.com','EmployeeStreet9',25000.00),('P003',0,'Employee5','1990-11-04','employee5@gmail.com','EmployeeStreet5',25000.00);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `had_test`
--

DROP TABLE IF EXISTS `had_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `had_test` (
  `patient_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`patient_id`,`test_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `had_test_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`),
  CONSTRAINT `had_test_ibfk_2` FOREIGN KEY (`test_id`) REFERENCES `test` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `had_test`
--

LOCK TABLES `had_test` WRITE;
/*!40000 ALTER TABLE `had_test` DISABLE KEYS */;
INSERT INTO `had_test` VALUES (1,3),(2,1),(3,2);
/*!40000 ALTER TABLE `had_test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `in_patient`
--

DROP TABLE IF EXISTS `in_patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `in_patient` (
  `registered_id` int(11) NOT NULL,
  `arrival_date` datetime NOT NULL,
  `discharge_Date` datetime NOT NULL,
  `room_allotted` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`registered_id`),
  CONSTRAINT `in_patient_ibfk_1` FOREIGN KEY (`registered_id`) REFERENCES `patient` (`patient_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `in_patient`
--

LOCK TABLES `in_patient` WRITE;
/*!40000 ALTER TABLE `in_patient` DISABLE KEYS */;
INSERT INTO `in_patient` VALUES (1,'2003-04-03 00:00:00','2003-04-15 00:00:00',1),(2,'2003-05-30 00:00:00','2003-06-03 00:00:00',1),(3,'2003-11-24 00:00:00','2003-12-05 00:00:00',1);
/*!40000 ALTER TABLE `in_patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login_details`
--

DROP TABLE IF EXISTS `login_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_details` (
  `id` varchar(5) NOT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_details`
--

LOCK TABLES `login_details` WRITE;
/*!40000 ALTER TABLE `login_details` DISABLE KEYS */;
INSERT INTO `login_details` VALUES ('A001','admin','iamadmin'),('D001','doctor','1');
/*!40000 ALTER TABLE `login_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine`
--

DROP TABLE IF EXISTS `medicine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine` (
  `medicine_id` int(11) NOT NULL,
  `medicine_name` varchar(50) NOT NULL,
  `medicine_cost` decimal(5,2) NOT NULL,
  `medicine_stock` int(11) NOT NULL,
  PRIMARY KEY (`medicine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine`
--

LOCK TABLES `medicine` WRITE;
/*!40000 ALTER TABLE `medicine` DISABLE KEYS */;
INSERT INTO `medicine` VALUES (1,'medicine1',10.00,100),(2,'medicine2',20.00,200),(3,'medicine3',30.00,300);
/*!40000 ALTER TABLE `medicine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medicine_bill`
--

DROP TABLE IF EXISTS `medicine_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medicine_bill` (
  `bill_id` int(11) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`,`medicine_id`),
  KEY `medicine_id` (`medicine_id`),
  CONSTRAINT `medicine_bill_ibfk_1` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`),
  CONSTRAINT `medicine_bill_ibfk_2` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medicine_bill`
--

LOCK TABLES `medicine_bill` WRITE;
/*!40000 ALTER TABLE `medicine_bill` DISABLE KEYS */;
INSERT INTO `medicine_bill` VALUES (1,2),(2,3),(3,1);
/*!40000 ALTER TABLE `medicine_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nurse`
--

DROP TABLE IF EXISTS `nurse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nurse` (
  `nurse_id` varchar(5) NOT NULL,
  `count_patient` int(11) DEFAULT NULL,
  PRIMARY KEY (`nurse_id`),
  CONSTRAINT `nurse_ibfk_1` FOREIGN KEY (`nurse_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nurse`
--

LOCK TABLES `nurse` WRITE;
/*!40000 ALTER TABLE `nurse` DISABLE KEYS */;
INSERT INTO `nurse` VALUES ('N001',1),('N002',1),('N003',1);
/*!40000 ALTER TABLE `nurse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `phone_no` varchar(12) NOT NULL,
  `patient_name` varchar(50) NOT NULL,
  `arrival_date` date NOT NULL,
  `disease` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`patient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'1999-06-03','MALE','91383345','Patient1','2020-10-10','COVID-19'),(2,'2005-12-05','MALE','91435577','IRFAN','2021-01-01','COVID-19'),(3,'2015-10-06','FEMALE','91435577','Patient3','2020-04-01','COVID-19');
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `patient_history`
--

DROP TABLE IF EXISTS `patient_history`;
/*!50001 DROP VIEW IF EXISTS `patient_history`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `patient_history` (
  `patient_id` tinyint NOT NULL,
  `patient_name` tinyint NOT NULL,
  `doctor_id` tinyint NOT NULL,
  `name` tinyint NOT NULL,
  `medicine_name` tinyint NOT NULL,
  `test_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `patient_view`
--

DROP TABLE IF EXISTS `patient_view`;
/*!50001 DROP VIEW IF EXISTS `patient_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `patient_view` (
  `patient_id` tinyint NOT NULL,
  `patient_name` tinyint NOT NULL,
  `total_cost` tinyint NOT NULL,
  `total_test_cost` tinyint NOT NULL,
  `total_medicine_cost` tinyint NOT NULL,
  `other_cost` tinyint NOT NULL,
  `room_cost` tinyint NOT NULL,
  `doctor_id` tinyint NOT NULL,
  `nurse_id` tinyint NOT NULL,
  `room_id` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pays`
--

DROP TABLE IF EXISTS `pays`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pays` (
  `patient_id` int(11) DEFAULT NULL,
  `bill_id` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `patient_id` (`patient_id`),
  CONSTRAINT `pays_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`),
  CONSTRAINT `pays_ibfk_2` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pays`
--

LOCK TABLES `pays` WRITE;
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
INSERT INTO `pays` VALUES (1,1),(2,2),(3,3);
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pharmacy`
--

DROP TABLE IF EXISTS `pharmacy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pharmacy` (
  `pharmacist_id` varchar(5) NOT NULL,
  `pharmacist_name` varchar(50) NOT NULL,
  PRIMARY KEY (`pharmacist_id`),
  CONSTRAINT `pharmacy_ibfk_1` FOREIGN KEY (`pharmacist_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pharmacy`
--

LOCK TABLES `pharmacy` WRITE;
/*!40000 ALTER TABLE `pharmacy` DISABLE KEYS */;
INSERT INTO `pharmacy` VALUES ('P001','pharmacist1'),('P002','pharmacist2'),('P003','pharmacist3');
/*!40000 ALTER TABLE `pharmacy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prescription` (
  `patient_id` int(11) NOT NULL,
  `pharmacist_id` varchar(5) NOT NULL,
  PRIMARY KEY (`patient_id`,`pharmacist_id`),
  KEY `pharmacist_id` (`pharmacist_id`),
  CONSTRAINT `prescription_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patient` (`patient_id`),
  CONSTRAINT `prescription_ibfk_2` FOREIGN KEY (`pharmacist_id`) REFERENCES `pharmacy` (`pharmacist_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prescription`
--

LOCK TABLES `prescription` WRITE;
/*!40000 ALTER TABLE `prescription` DISABLE KEYS */;
INSERT INTO `prescription` VALUES (1,'P003'),(2,'P002'),(3,'P001');
/*!40000 ALTER TABLE `prescription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provides`
--

DROP TABLE IF EXISTS `provides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provides` (
  `pharmacist_id` varchar(5) NOT NULL,
  `medicine_id` int(11) NOT NULL,
  PRIMARY KEY (`pharmacist_id`,`medicine_id`),
  KEY `medicine_id` (`medicine_id`),
  CONSTRAINT `provides_ibfk_1` FOREIGN KEY (`pharmacist_id`) REFERENCES `pharmacy` (`pharmacist_id`),
  CONSTRAINT `provides_ibfk_2` FOREIGN KEY (`medicine_id`) REFERENCES `medicine` (`medicine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provides`
--

LOCK TABLES `provides` WRITE;
/*!40000 ALTER TABLE `provides` DISABLE KEYS */;
INSERT INTO `provides` VALUES ('P001',3),('P002',1),('P003',2);
/*!40000 ALTER TABLE `provides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_bill`
--

DROP TABLE IF EXISTS `room_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_bill` (
  `bill_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`bill_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `room_bill_ibfk_1` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`room_id`),
  CONSTRAINT `room_bill_ibfk_2` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_bill`
--

LOCK TABLES `room_bill` WRITE;
/*!40000 ALTER TABLE `room_bill` DISABLE KEYS */;
INSERT INTO `room_bill` VALUES (3,1),(1,2),(2,3);
/*!40000 ALTER TABLE `room_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `room_id` int(11) NOT NULL,
  `room_type` varchar(50) DEFAULT NULL,
  `room_cost` decimal(10,2) DEFAULT NULL,
  `is_available` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'3-star',10.00,1),(2,'4-star',15.00,1),(3,'3-star',10.00,1);
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `test_id` int(11) NOT NULL,
  `test_name` varchar(20) NOT NULL,
  `test_cost` decimal(6,2) DEFAULT 0.00,
  PRIMARY KEY (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'test1',10.00),(2,'test2',10.00),(3,'test3',10.00);
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_bill`
--

DROP TABLE IF EXISTS `test_bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_bill` (
  `test_id` int(11) NOT NULL,
  `bill_id` int(11) NOT NULL,
  PRIMARY KEY (`bill_id`,`test_id`),
  KEY `test_id` (`test_id`),
  CONSTRAINT `test_bill_ibfk_1` FOREIGN KEY (`test_id`) REFERENCES `test` (`test_id`),
  CONSTRAINT `test_bill_ibfk_2` FOREIGN KEY (`bill_id`) REFERENCES `bill` (`bill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_bill`
--

LOCK TABLES `test_bill` WRITE;
/*!40000 ALTER TABLE `test_bill` DISABLE KEYS */;
INSERT INTO `test_bill` VALUES (3,1),(1,2),(2,3);
/*!40000 ALTER TABLE `test_bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `works`
--

DROP TABLE IF EXISTS `works`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `works` (
  `department_id` int(11) DEFAULT NULL,
  `employee_id` varchar(5) NOT NULL,
  PRIMARY KEY (`employee_id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `works_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`),
  CONSTRAINT `works_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `works_ibfk_3` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `works`
--

LOCK TABLES `works` WRITE;
/*!40000 ALTER TABLE `works` DISABLE KEYS */;
INSERT INTO `works` VALUES (1,'D001'),(1,'D002'),(1,'D003'),(2,'N001'),(2,'N002'),(2,'N003'),(3,'P001'),(3,'P002'),(3,'P003');
/*!40000 ALTER TABLE `works` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `patient_history`
--

/*!50001 DROP TABLE IF EXISTS `patient_history`*/;
/*!50001 DROP VIEW IF EXISTS `patient_history`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `patient_history` AS (select `patient`.`patient_id` AS `patient_id`,`patient`.`patient_name` AS `patient_name`,`doctors`.`doctor_id` AS `doctor_id`,`employee`.`name` AS `name`,`medicine`.`medicine_name` AS `medicine_name`,`test`.`test_name` AS `test_name` from (((((((((`patient` join `assigned_doctor` on(`patient`.`patient_id` = `assigned_doctor`.`patient_id`)) join `doctors` on(`assigned_doctor`.`doctor_id` = `doctors`.`doctor_id`)) join `had_test` on(`patient`.`patient_id` = `had_test`.`patient_id`)) join `test` on(`had_test`.`test_id` = `test`.`test_id`)) join `prescription` on(`patient`.`patient_id` = `prescription`.`patient_id`)) join `pharmacy` on(`prescription`.`pharmacist_id` = `pharmacy`.`pharmacist_id`)) join `provides` on(`prescription`.`pharmacist_id` = `provides`.`pharmacist_id`)) join `medicine` on(`provides`.`medicine_id` = `medicine`.`medicine_id`)) join `employee` on(`employee`.`employee_id` = `doctors`.`doctor_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `patient_view`
--

/*!50001 DROP TABLE IF EXISTS `patient_view`*/;
/*!50001 DROP VIEW IF EXISTS `patient_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `patient_view` AS (select `patient`.`patient_id` AS `patient_id`,`patient`.`patient_name` AS `patient_name`,`bill`.`total_cost` AS `total_cost`,sum(`bill`.`test_cost`) AS `total_test_cost`,sum(`bill`.`medicine_cost`) AS `total_medicine_cost`,`bill`.`other_cost` AS `other_cost`,`bill`.`room_cost` AS `room_cost`,`assigned_doctor`.`doctor_id` AS `doctor_id`,`assigned_nurse`.`nurse_id` AS `nurse_id`,`allocated`.`room_id` AS `room_id` from ((((((`patient` left join `in_patient` on(`patient`.`patient_id` = `in_patient`.`registered_id`)) join `assigned_nurse` on(`in_patient`.`registered_id` = `assigned_nurse`.`registered_id`)) join `assigned_doctor` on(`patient`.`patient_id` = `assigned_doctor`.`patient_id`)) join `pays` on(`patient`.`patient_id` = `pays`.`patient_id`)) join `bill` on(`pays`.`bill_id` = `bill`.`bill_id`)) join `allocated` on(`in_patient`.`registered_id` = `allocated`.`registered_id`)) group by `patient`.`patient_id`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-27  1:21:41
